package org.grakovne.threatmodeler.simulation.engine;

import org.grakovne.threatmodeler.common.DiceRoll;
import org.grakovne.threatmodeler.entity.Protection;
import org.grakovne.threatmodeler.entity.Threat;
import org.grakovne.threatmodeler.entity.Unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SimulationEnvironment {
    private int currentTimeUnit = 0;
    private int eventCounter = 0;

    private List<Unit> units;

    private DiceRoll diceRoll;

    private Map<Unit, List<Threat>> threatMapped;
    private Map<Unit, List<Protection>> protectionMapped;

    private int summaryDamage = 0;

    public SimulationEnvironment(DiceRoll diceRoll,
                                 List<Unit> units,
                                 Map<Unit, List<Threat>> threatMapped) {

        this(diceRoll, units, threatMapped, new HashMap<>());

    }

    public SimulationEnvironment(DiceRoll diceRoll,
                                 List<Unit> units,
                                 Map<Unit, List<Threat>> threatMapped,
                                 Map<Unit, List<Protection>> protectionMapped) {

        this.units = new ArrayList<>(units.size());
        for (Unit unit : units) {
            this.units.add(new Unit(unit.getName(), unit.getCost()));
        }

        this.threatMapped = threatMapped;
        this.protectionMapped = protectionMapped;

        this.diceRoll = diceRoll;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public int getCurrentTimeUnit() {
        return currentTimeUnit;
    }

    public void threatUnits(DiceRoll diceRoll) {
        this.diceRoll = diceRoll;
        eventCounter = 0;

        for (Unit unit : units) {
            threadUnit(unit);
        }
        currentTimeUnit++;
    }

    public int getSummaryDamage() {
        return summaryDamage;
    }

    private void threadUnit(Unit unit) {
        List<Threat> unitThreats = threatMapped.get(unit);

        if (null == unitThreats || unitThreats.size() == 0) {
            return;
        }

        long actualCost = unit.getCost();

        for (Threat threat : unitThreats) {
            if (isEventHappen(threat.getProbabilityPercent())) {
                long summaryDamagePercent = threat.getDamagePercent();

                List<Protection> supportedProtections = getSupportedProtections(unit, threat);
                for (Protection protection : supportedProtections) {
                    if (isEventHappen(protection.getProbabilityPercent())) {
                        summaryDamagePercent = summaryDamagePercent * protection.getConservationFactorPercent() / 100;
                    }
                }

                actualCost -= summaryDamagePercent;

                summaryDamage += summaryDamagePercent;
            }
        }

        if (actualCost < 0) {
            actualCost = 0;
        }

        unit.setCost(actualCost);
    }

    public long getSummaryCost() {
        long result = 0;

        for (Unit unit : units) {
            result += unit.getCost();
        }

        return result;
    }

    private boolean isEventHappen(int probability) {
        eventCounter++;
        return probability >= diceRoll.getByPosition(eventCounter);
    }

    private List<Protection> getSupportedProtections(Unit unit, Threat threat) {
        if (null == protectionMapped.get(unit)) {
            return new ArrayList<>();
        }

        Predicate<Protection> protectionPredicate = protection -> protection.getCoveredThreat().equals(threat);
        return new ArrayList<>(protectionMapped.get(unit)).stream().filter(protectionPredicate).collect(Collectors.toList());
    }
}
