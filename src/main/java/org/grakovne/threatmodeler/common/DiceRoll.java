package org.grakovne.threatmodeler.common;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class DiceRoll {
    private final int MAXIMAL_NUMBER = 100;
    private final int SEQUENCE_SIZE = 65_535;

    private List<Integer> numbers;

    public DiceRoll() {
        throwDice();
    }

    private void throwDice() {
        SecureRandom random = new SecureRandom();
        numbers = new ArrayList<>(SEQUENCE_SIZE);

        for (int i = 0; i < SEQUENCE_SIZE; i++) {
            numbers.add(Math.abs(random.nextInt(MAXIMAL_NUMBER)));
        }
    }

    public int getByPosition(int position) {
        return numbers.get(position % SEQUENCE_SIZE);
    }
}
