package org.grakovne.threatmodeler.factories;

import org.grakovne.threatmodeler.entity.Unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UnitFactory {
    private static final String UNIT_NAME_PREFIX = "Unit__";
    private static final int MAXIMAL_UNIT_COST = 1000;

    public List<Unit> produceUnitList(int number) {
        Random random = new Random();
        List<Unit> result = new ArrayList<Unit>(number);

        for (int i = 0; i < number; i++) {
            result.add(new Unit(UNIT_NAME_PREFIX + (i + 1), Math.abs(random.nextInt(MAXIMAL_UNIT_COST))));
        }

        return result;
    }
}
