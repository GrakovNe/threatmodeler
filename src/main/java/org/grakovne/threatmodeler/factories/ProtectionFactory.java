package org.grakovne.threatmodeler.factories;

import org.grakovne.threatmodeler.entity.Protection;
import org.grakovne.threatmodeler.entity.Threat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProtectionFactory {
    private static final int MAXIMAL_CONSERVATION_PERCENT = 90;
    private static final int MAXIMAL_PROBABILITY_PERCENT = 90;

    private static final int MAXIMAL_PROTECTION_COST = 50;

    public List<Protection> produceProtectionList(int number, List<Threat> threats) {
        Random random = new Random();
        List<Protection> result = new ArrayList<Protection>(number);

        for (int i = 0; i < number; i++) {
            Threat coveredThreat = threats.get(Math.abs(random.nextInt(threats.size())));
            result.add(new Protection(Math.abs(random.nextInt(MAXIMAL_PROTECTION_COST)), coveredThreat, Math.abs(random.nextInt(MAXIMAL_CONSERVATION_PERCENT)), Math.abs(random.nextInt(MAXIMAL_PROBABILITY_PERCENT))));
        }

        return result;
    }
}
