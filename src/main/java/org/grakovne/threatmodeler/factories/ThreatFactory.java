package org.grakovne.threatmodeler.factories;

import org.grakovne.threatmodeler.entity.Threat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ThreatFactory {
    private static final int MAXIMAL_PROBABILITY_PERCENT = 90;
    private static final int MAXIMAL_DAMAGE_PERCENT = 90;

    public List<Threat> produceThreatList(int number) {
        Random random = new Random();
        List<Threat> result = new ArrayList<Threat>(number);

        for (int i = 0; i < number; i++) {
            result.add(new Threat(Math.abs(random.nextInt(MAXIMAL_PROBABILITY_PERCENT)), Math.abs(random.nextInt(MAXIMAL_DAMAGE_PERCENT))));
        }

        return result;
    }
}
