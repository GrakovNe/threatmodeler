package org.grakovne.threatmodeler.entity;

public class Protection {
    private int cost;

    private int conservationFactorPercent;
    private int probabilityPercent;
    private Threat coveredThreat;

    public Protection(int cost, Threat coveredThreat, int conservationFactorPercent, int probabilityPercent) {
        setCost(cost);
        setCoveredThreat(coveredThreat);
        setProbabilityPercent(probabilityPercent);
        setConservationFactorPercent(conservationFactorPercent);
    }

    public int getConservationFactorPercent() {
        return conservationFactorPercent;
    }

    public void setConservationFactorPercent(int conservationFactorPercent) {
        this.conservationFactorPercent = conservationFactorPercent;
    }

    public int getProbabilityPercent() {
        return probabilityPercent;
    }

    public void setProbabilityPercent(int probabilityPercent) {
        this.probabilityPercent = probabilityPercent;
    }

    public Threat getCoveredThreat() {
        return coveredThreat;
    }

    public void setCoveredThreat(Threat coveredThreat) {
        this.coveredThreat = coveredThreat;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Protection{" +
                "cost=" + cost +
                ", conservationFactorPercent=" + conservationFactorPercent +
                ", probabilityPercent=" + probabilityPercent +
                ", coveredThreat=" + coveredThreat +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Protection that = (Protection) o;

        if (cost != that.cost) return false;
        if (conservationFactorPercent != that.conservationFactorPercent) return false;
        if (probabilityPercent != that.probabilityPercent) return false;
        return coveredThreat != null ? coveredThreat.equals(that.coveredThreat) : that.coveredThreat == null;
    }

    @Override
    public int hashCode() {
        int result = cost;
        result = 31 * result + conservationFactorPercent;
        result = 31 * result + probabilityPercent;
        result = 31 * result + (coveredThreat != null ? coveredThreat.hashCode() : 0);
        return result;
    }
}
