package org.grakovne.threatmodeler.entity;

public class Threat {
    private int probabilityPercent;
    private long damagePercent;

    public Threat(int probabilityPercent, long damagePercent) {
        setDamagePercent(damagePercent);
        setProbabilityPercent(probabilityPercent);
    }

    public int getProbabilityPercent() {
        return probabilityPercent;
    }

    public void setProbabilityPercent(int probabilityPercent) {
        this.probabilityPercent = probabilityPercent;
    }

    public long getDamagePercent() {
        return damagePercent;
    }

    public void setDamagePercent(long damagePercent) {
        this.damagePercent = damagePercent;
    }

    @Override
    public String toString() {
        return "Threat{" +
                "probabilityPercent=" + probabilityPercent +
                ", damagePercent=" + damagePercent +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Threat threat = (Threat) o;

        if (probabilityPercent != threat.probabilityPercent) return false;
        return damagePercent == threat.damagePercent;
    }

    @Override
    public int hashCode() {
        int result = probabilityPercent;
        result = 31 * result + (int) (damagePercent ^ (damagePercent >>> 32));
        return result;
    }
}
