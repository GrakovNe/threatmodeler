package org.grakovne.threatmodeler.mappers;

import org.grakovne.threatmodeler.entity.Protection;
import org.grakovne.threatmodeler.entity.Threat;
import org.grakovne.threatmodeler.entity.Unit;

import java.util.*;

public class UnitMapper {
    private static List<Protection> selectRandomProtection(List<Protection> protections) {
        Random random = new Random();

        int selectedThreadsProtections = random.nextInt(protections.size());
        List<Protection> shuffledProtections = new ArrayList<>(protections);

        Collections.shuffle(shuffledProtections);
        return shuffledProtections.subList(0, selectedThreadsProtections);
    }

    public Map<Unit, List<Threat>> mapRandomThread(List<Unit> units, List<Threat> threats) {
        Map<Unit, List<Threat>> result = new HashMap<>();

        for (Unit unit : units) {
            result.put(unit, selectRandomThreats(threats));
        }

        return result;
    }

    public Map<Unit, List<Protection>> mapRandomProtection(List<Unit> units, List<Protection> protections) {
        Map<Unit, List<Protection>> result = new HashMap<>();

        for (Unit unit : units) {
            result.put(unit, selectRandomProtection(protections));
        }

        return result;
    }

    private List<Threat> selectRandomThreats(List<Threat> threats) {
        Random random = new Random();

        int selectedThreadsNumber = random.nextInt(threats.size());
        List<Threat> shuffledThreats = new ArrayList<>(threats);

        Collections.shuffle(shuffledThreats);
        return shuffledThreats.subList(0, selectedThreadsNumber);
    }
}
