package org.grakovne.threatmodeler.ui.activities;

import javax.swing.*;
import java.awt.*;

public class MainActivity extends JFrame {

    private JLabel unitNumberLabel;
    private JTextField unitNumberField;

    private JLabel threatsNumberLabel;
    private JTextField threatsNumberField;

    private JLabel protectionNumberLabel;
    private JTextField protectionNumberField;

    private JLabel timeUnitsLabel;
    private JTextField timeUnitsField;

    private JButton generateButton;

    private JTextArea reportArea;
    private JScrollPane reportAreaScrollPane;

    public MainActivity() {
        super("THREATS MODELER");

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(300, 50, 585, 145);
        setLayout(null);

        unitNumberLabel = new JLabel("Объектов:");
        unitNumberLabel.setBounds(20, 20, 120, 20);
        add(unitNumberLabel);

        unitNumberField = new JTextField("15");
        unitNumberField.setBounds(20, 40, 120, 20);
        add(unitNumberField);

        threatsNumberLabel = new JLabel("Угроз:");
        threatsNumberLabel.setBounds(160, 20, 120, 20);
        add(threatsNumberLabel);

        threatsNumberField = new JTextField("10");
        threatsNumberField.setBounds(160, 40, 120, 20);
        add(threatsNumberField);

        protectionNumberLabel = new JLabel("Протекторов:");
        protectionNumberLabel.setBounds(300, 20, 120, 20);
        add(protectionNumberLabel);

        protectionNumberField = new JTextField("25");
        protectionNumberField.setBounds(300, 40, 120, 20);
        add(protectionNumberField);

        timeUnitsLabel = new JLabel("Времени:");
        timeUnitsLabel.setBounds(440, 20, 120, 20);
        add(timeUnitsLabel);

        timeUnitsField = new JTextField("10");
        timeUnitsField.setBounds(440, 40, 120, 20);
        add(timeUnitsField);

        generateButton = new JButton("Сделать хорошо!");
        generateButton.setBounds(190, 80, 200, 25);
        add(generateButton);

        reportArea = new JTextArea();
        reportArea.setFont(new Font("arial", Font.PLAIN, 12));
        //add(reportArea);

        reportAreaScrollPane = new JScrollPane(reportArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        reportAreaScrollPane.setBounds(20, 120, 540, 330);
        add(reportAreaScrollPane);

        setResizable(false);
    }

    public void expandWindow() {
        setBounds(300, 50, 585, 500);
    }

    public int getUnits() {
        return Integer.parseInt(unitNumberField.getText());
    }

    public int getThreats() {
        return Integer.parseInt(threatsNumberField.getText());
    }

    public int getProtections() {
        return Integer.parseInt(protectionNumberField.getText());
    }

    public int getTimeUnits() {
        return Integer.parseInt(timeUnitsField.getText());
    }

    public JButton getGenerateButton() {
        return generateButton;
    }

    public void clearReportArea() {
        reportArea.setText("");
    }

    public void addReportString(String string) {
        reportArea.setText(reportArea.getText() + string + "\n");
        reportArea.setCaretPosition(reportArea.getDocument().getLength());
    }
}
