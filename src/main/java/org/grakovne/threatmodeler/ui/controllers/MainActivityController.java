package org.grakovne.threatmodeler.ui.controllers;

import org.grakovne.threatmodeler.common.DiceRoll;
import org.grakovne.threatmodeler.entity.Protection;
import org.grakovne.threatmodeler.entity.Threat;
import org.grakovne.threatmodeler.entity.Unit;
import org.grakovne.threatmodeler.factories.ProtectionFactory;
import org.grakovne.threatmodeler.factories.ThreatFactory;
import org.grakovne.threatmodeler.factories.UnitFactory;
import org.grakovne.threatmodeler.mappers.UnitMapper;
import org.grakovne.threatmodeler.simulation.engine.SimulationEnvironment;
import org.grakovne.threatmodeler.ui.activities.MainActivity;
import org.grakovne.threatmodeler.ui.views.PlotView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivityController {
    private final MainActivity mainActivity;

    private List<Unit> units;
    private List<Threat> threats;
    private List<Protection> protections;

    private Map<Unit, List<Threat>> threatMapped;
    private Map<Unit, List<Protection>> protectionMapped;


    public MainActivityController() {
        this.mainActivity = new MainActivity();
        initWindow();
    }

    private void initWindow() {

        mainActivity.getGenerateButton().addActionListener(e -> {
            new Thread(() -> runSimulation()).start();

        });

        mainActivity.setVisible(true);
    }

    private void runSimulation() {
        mainActivity.clearReportArea();
        mainActivity.expandWindow();

        mainActivity.addReportString("Готовим жизненое пространство...");
        System.gc();

        long startTime = System.currentTimeMillis();

        mainActivity.addReportString("Генерируем объекты...");

        units = new UnitFactory().produceUnitList(mainActivity.getUnits());
        threats = new ThreatFactory().produceThreatList(mainActivity.getThreats());
        protections = new ProtectionFactory().produceProtectionList(mainActivity.getProtections(), threats);

        threatMapped = new UnitMapper().mapRandomThread(units, threats);
        protectionMapped = new UnitMapper().mapRandomProtection(units, protections);

        DiceRoll diceRoll = new DiceRoll();

        SimulationEnvironment freeEnvironment = new SimulationEnvironment(diceRoll, units, threatMapped);
        SimulationEnvironment protectedEnvironment = new SimulationEnvironment(diceRoll, units, threatMapped, protectionMapped);

        long summaryUnitCostBeforeSimulation = getUnitListCost(units);

        mainActivity.addReportString("Генерация объектов завершена!");
        mainActivity.addReportString("Затрачено времени: " + (System.currentTimeMillis() - startTime) + " ms");
        mainActivity.addReportString("====");
        mainActivity.addReportString("");

        mainActivity.addReportString("Рассматриваем полученные объекты...");

        for (Unit unit : units) {
            mainActivity.addReportString("Имя: " + unit.getName());
            mainActivity.addReportString("Стоимость: " + unit.getCost());
            mainActivity.addReportString("");
            mainActivity.addReportString("Возможные угрозы:");

            int counter = 1;
            for (Threat threat : threatMapped.get(unit)) {
                mainActivity.addReportString("");
                mainActivity.addReportString("\t" + "Номер угрозы: " + counter);
                mainActivity.addReportString("\t" + "Наносимый ущерб: " + threat.getDamagePercent());
                mainActivity.addReportString("\t" + "Вероятность возникновения: " + threat.getProbabilityPercent() + "%");
                counter++;
            }

            mainActivity.addReportString("");
            mainActivity.addReportString("Установленные протекторы:");

            counter = 1;
            for (Protection protection : protectionMapped.get(unit)) {
                mainActivity.addReportString("");
                mainActivity.addReportString("\t" + "Номер протектора: " + counter);
                mainActivity.addReportString("\t" + "Стоимость: " + protection.getCost());
                mainActivity.addReportString("\t" + "Смягчающее воздействие: " + protection.getConservationFactorPercent() + "%");
                mainActivity.addReportString("\t" + "Вероятность срабатывания: " + protection.getProbabilityPercent() + "%");
                counter++;
            }

            mainActivity.addReportString("=====");
            mainActivity.addReportString("");
        }

        mainActivity.addReportString("Начинаем атаки...");

        startTime = System.currentTimeMillis();


        List<Integer> freeSummaryDamage = new ArrayList<>();
        List<Integer> protectedSummaryDamage = new ArrayList<>();

        for (int i = 0; i < mainActivity.getTimeUnits(); i++) {
            diceRoll = new DiceRoll();

            protectedEnvironment.threatUnits(diceRoll);
            protectedSummaryDamage.add(protectedEnvironment.getSummaryDamage());

            freeEnvironment.threatUnits(diceRoll);
            freeSummaryDamage.add(freeEnvironment.getSummaryDamage());

        }

        mainActivity.addReportString("Симуляция атак завершена!");
        mainActivity.addReportString("Затрачено времени: " + (System.currentTimeMillis() - startTime) + " ms");
        mainActivity.addReportString("====");
        mainActivity.addReportString("");

        mainActivity.addReportString("Общая стоимость объектов до симуляции: " + summaryUnitCostBeforeSimulation);
        mainActivity.addReportString("Общая стоимость поврежденных объектов без протекции: " + getUnitListCost(freeEnvironment.getUnits()));
        mainActivity.addReportString("Общая стоимость поврежденных объектов под протекцией: " + getUnitListCost(protectedEnvironment.getUnits()));
        mainActivity.addReportString("");

        mainActivity.addReportString("Общая стоимость протекции: " + getProtectionsListCost(protections));
        long unitCostDifference = getUnitListCost(protectedEnvironment.getUnits()) - getUnitListCost(freeEnvironment.getUnits());
        mainActivity.addReportString("Экономическая эффективность протекции: " + (unitCostDifference - getProtectionsListCost(protections)));

        mainActivity.addReportString("");
        mainActivity.addReportString("Строим графики...");

        PlotView.createPlot(freeSummaryDamage, "Summary damage without protection");
        PlotView.createPlot(protectedSummaryDamage, "Summary damage with protection");

        mainActivity.addReportString("Выдыхаем.");
    }

    private long getUnitListCost(List<Unit> units) {
        long result = 0;

        for (Unit unit : units) {
            result += unit.getCost();
        }

        return result;
    }

    private long getProtectionsListCost(List<Protection> protections) {
        long result = 0;

        for (Protection protection : protections) {
            result += protection.getCost();
        }

        return result;
    }


}
